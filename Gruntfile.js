module.exports = function(grunt) {

	var jsFileList = ['hippocket/js/*.js'];
	var cssFileList = ['hippocket/css/*.css'];

	grunt.initConfig({
		jshint: {
			files: ['Gruntfile.js', 'hippocket/js/hipscript.js'],
			options: {
				globals: {
					jQuery: true
				}
			}
		},
		concat: {
			options: {
				separator: ';',
			},
			dist: {
				src: [jsFileList],
				dest: 'hippocket/assets/main.js',
			},
		},
		concat_css: {
			options: {},
			all: {
				src: [cssFileList],
				dest: 'hippocket/assets/main.css',
			},
		},
		uglify: {
			dist: {
				files: {
					'hippocket/assets/main.min.js': 'hippocket/assets/main.js'
				}
			}
		},
		cssmin: {
			dist: {
				files: {
					'hippocket/assets/main.min.css': 'hippocket/assets/main.css'
				}
			}
		},
		watch: {
			files: ['<%= jshint.files %>'],
			tasks: ['jshint']
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-concat-css');
	grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'concat_css', 'cssmin']);
};