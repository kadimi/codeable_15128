<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to hippocket_comment() which is
 * located in the functions.php file.
 *
 * @package hippocket
 * @since hippocket 1.0
 */
?>

<?php
	/*
	 * If the current post is protected by a password and
	 * the visitor has not yet entered the password we will
	 * return early without loading the comments.
	 */
	if ( post_password_required() )
		return;
?>

	<div id="comments" class="comments-area<?php echo is_user_logged_in() ? ' logged-in' : ''; ?>">
					
	<?php if ( have_comments() ) : ?>
	    <h3 class="comments-head">
			<?php
				printf( _nx( '1 Comment', '%1$s Comments', get_comments_number(), 'comments title', 'hippocket' ), get_comments_number() );
			?>
		</h3>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav role="navigation" id="comment-nav-above" class="site-navigation comment-navigation">
			<h1 class="assistive-text"><?php _e( 'Comment navigation', 'hippocket' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'hippocket' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'hippocket' ) ); ?></div>
		</nav><!-- #comment-nav-before .site-navigation .comment-navigation -->
		<?php endif; // check for comment navigation ?>

		 <section class="comments-block">
			<?php
				wp_list_comments('type=comment&callback=hippocket_comment');
			?>
		</section><!-- .commentlist -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav role="navigation" id="comment-nav-below" class="site-navigation comment-navigation">
			<h1 class="assistive-text"><?php _e( 'Comment navigation', 'hippocket' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'hippocket' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'hippocket' ) ); ?></div>
		</nav><!-- #comment-nav-below .site-navigation .comment-navigation -->
		<?php endif; // check for comment navigation ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="nocomments"><?php _e( 'Comments are closed.', 'hippocket' ); ?></p>
	<?php endif; ?>

	<?php  
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		
		$fields = array(
		  'author' => '<div class="row"> <div class="form-group col-md-4 col-sm-4"><input type="text" class="form-control input-lg" value="' . esc_attr( $commenter['comment_author'] ) . '" placeholder="Your name" '. $aria_req .'></div>',
		  'email' => '<div class="form-group col-md-4 col-sm-4"><input type="email" class="form-control input-lg" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" placeholder="Your email" '. $aria_req .'></div>',
		  'url' => '<div class="form-group col-md-4 col-sm-4"><input type="url" class="form-control input-lg" value="' . esc_attr( $commenter['comment_author_url'] ) . '" placeholder="Website (optional)"></div></div>',
		);
		$comments_args = array(
			'fields' =>  apply_filters( 'comment_form_default_fields', $fields ),
			'title_reply'       => __( 'Add your comment' ),
			'title_reply_to'    => __( 'Add your comment to %s' ),
			'cancel_reply_link' => __( 'Cancel your comment' ),
			'label_submit'      => __( 'Add your comment' ),
			'comment_field' => '<div class="row"><div class="form-group col-md-12"><textarea name="comment" cols="8" rows="4" class="form-control input-lg" placeholder="Your comment" '. $aria_req .'></textarea></div></div>',
		);
	?>	
	<?php comment_form($comments_args); ?>

</div><!-- #comments .comments-area -->
