<!-- Container -->
<?php global $hip_options; ?>
<div class="container">
  <section class="row breath">
    <div class="col-md-12 footerlinks">  
		<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'footer-menu' ) ); ?>
		
		<div class="sociallinks">
			<ul>
				<li class="twitter"><a href="<?php echo $hip_options['opt-twitter-url']; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/twitter-32x32.png" alt="twitter"></a></li>
				<li class="facebook"><a href="<?php echo $hip_options['opt-facebook-url']; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/facebook-32x32.png" alt="facebook"></a></li>
				<li class="linkedin"><a href="<?php echo $hip_options['opt-linkedin-url']; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/linked-in-32x32.png" alt="linkedin"></a></li>
			</ul>
		</div>
		<div class="col-md-6"> 
			<img class="pull-left" src="<?php bloginfo('template_directory') ?>/images/HipPocket_HorizontalLogo.png" alt="horizontal hippocket logo" width="313px" height="109px">
		</div>
		<div class="col-md-6"> 
			<!-- Begin MailChimp Signup Form -->
			<link href="//cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css">
			<style type="text/css">
				#mc_embed_signup{background:#fff; clear:right; font:14px 'Open Sans Condensed Light;  /*width:350px;*/}
				/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
				   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
			</style>
			<div id="mc_embed_signup" class="pull-right">
			<form action="//hippocket.us10.list-manage.com/subscribe/post?u=cf764307e4e93c694bbee2687&amp;id=efbb0fa323" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				<div id="mc_embed_signup_scroll">
				<label for="mce-EMAIL">Subscribe to our mailing list</label>
				<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
				<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				<input type="hidden" name="b_cf764307e4e93c694bbee2687_efbb0fa323" tabindex="-1" value="">
				<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
				</div>
			</form>
			</div>
			<!--End mc_embed_signup-->
		</div>
		<p style="display: inline-block;">&copy; <?php echo date("Y"); ?> <?php _e('HipPocket Inc. All Rights Reserved'); ?></p>
    </div>
  </section>
  <!-- // End Client Logos --> 
  
</div>
<!-- // Container Ends --> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59569454-1', 'auto');
  ga('send', 'pageview');

</script>
<?php wp_footer(); ?>
</body>
</html>