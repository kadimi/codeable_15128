<?php
function hippocket_setup() {

	/*
	 * Theme options
	 *
	 */
	if ( !class_exists( 'options' ) && file_exists( dirname( __FILE__ ) . '/options/ReduxCore/framework.php' ) ) {
		require_once( dirname( __FILE__ ) . '/options/ReduxCore/framework.php' );
	}
	if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/options/sample/sample-config.php' ) ) {
		require_once( dirname( __FILE__ ) . '/options/sample/sample-config.php' );
		include( dirname( __FILE__ ) . '/options/meta/demo.php' );
	}


	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on hippocket, use a find and replace
	 * to change 'hippocket' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'hippocket', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );
	add_image_size( 'thumb-small', 50, 50 );
	add_image_size( 'thumb-medium', 150, 150 );
	add_image_size( 'thumb-large', 300, 300 );
	add_image_size( 'thumb-about', 200, 200 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'hippocket' ),
		'footer'  => __( 'Footer Menu', 'hippocket' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );
}
add_action( 'after_setup_theme', 'hippocket_setup' );

/**
 * Register widget area.
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function hippocket_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog', 'hippocket' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'hippocket' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'General pages', 'hippocket' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'hippocket' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'About us', 'hippocket' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'hippocket' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'hippocket_widgets_init' );

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function hippocket_scripts() {

	if ( TRUE ) {

		wp_enqueue_style('main', get_template_directory_uri() . '/assets/main.min.css', array(), null, "all");
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', array(), null, true );
		wp_enqueue_script('main', get_template_directory_uri() . '/assets/main.min.js',  array('jquery'), null, TRUE);

		return;
	}


	// CSS
	wp_enqueue_style('bootstrapcss',    get_template_directory_uri() . '/css/bootstrap.css', array(), false, "all");
	wp_enqueue_style('startup',         get_template_directory_uri() . '/css/startup.css',   array(), false, "all");

	// jQuery
	wp_deregister_script('jquery' );  
	wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', array(), null, true ); 

	// JS	
	wp_enqueue_script('bootstrap',               get_template_directory_uri() . '/js/bootstrap.js',               array('jquery'), null, true);
	wp_enqueue_script('easing',                  get_template_directory_uri() . '/js/easing.js',                  array('jquery'), null, true);
	wp_enqueue_script('grids',                   get_template_directory_uri() . '/js/grids.min.js',               array('jquery'), null, true);
	wp_enqueue_script('html5shiv',               get_template_directory_uri() . '/js/html5shiv.js',               array('jquery'), null, true);
	wp_enqueue_script('jquery-responsiveTables', get_template_directory_uri() . '/js/jquery-responsiveTables.js', array('jquery'), null, true);
	wp_enqueue_script('responsiveText',          get_template_directory_uri() . '/js/jquery.responsiveText.js',   array('jquery'), null, true);
	wp_enqueue_script('respondmin',              get_template_directory_uri() . '/js/respond.min.js',             array('jquery'), null, true);
	wp_enqueue_script('hippace',                 get_template_directory_uri() . '/js/pace.js',                    array('jquery'), null, true);
	wp_enqueue_script('hiptyper',                get_template_directory_uri() . '/js/typer.js',                   array('jquery'), null, true);
	wp_enqueue_script('hipnicescroll',           get_template_directory_uri() . '/js/nicescroll.js',              array('jquery'), null, true);
	wp_enqueue_script('hippocketscript',         get_template_directory_uri() . '/js/hipscript.js',               array('jquery'), null, true);

	global $wp_scripts;
	$wp_scripts->add_data('html5shiv', 'conditional', 'lt IE 9');
	$wp_scripts->add_data('respondmin', 'conditional', 'lt IE 9');
}
add_action('wp_enqueue_scripts', 'hippocket_scripts');

/**
 * Twitter bootstrap menu walker
 *
*/
function hippocket_bootstrap_setup(){

	add_action( 'init', 'register_menu' );

	class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {


		function start_lvl( &$output, $depth ) {

			$indent = str_repeat( "\t", $depth );
			$output	   .= "\n$indent<ul class=\"dropdown-menu\">\n";

		}

		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			$li_attributes = '';
			$class_names = $value = '';

			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = ($args->has_children) ? 'dropdown' : '';
			$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
			$classes[] = 'menu-item-' . $item->ID;


			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			$class_names = ' class="' . esc_attr( $class_names ) . '"';

			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
			
			$prepend = '';
			$append = '';
			 
			if($depth != 0)
			   {
						 $description = $append = $prepend = "";
			   }

			$item_output = $args->before;
			$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ) .$append;
			$item_output .= $description.$args->link_after;
			$item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
			$item_output .= $args->after;

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}

		function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

			if ( !$element )
				return;

			$id_field = $this->db_fields['id'];

			//display this element
			if ( is_array( $args[0] ) ) 
				$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
			else if ( is_object( $args[0] ) ) 
				$args[0]->has_children = ! empty( $children_elements[$element->$id_field] ); 
			$cb_args = array_merge( array(&$output, $element, $depth), $args);
			call_user_func_array(array(&$this, 'start_el'), $cb_args);

			$id = $element->$id_field;

			// descend only when the depth is right and there are childrens for this element
			if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

				foreach( $children_elements[ $id ] as $child ){

					if ( !isset($newlevel) ) {
						$newlevel = true;
						//start the child delimiter
						$cb_args = array_merge( array(&$output, $depth), $args);
						call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
					}
					$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
				}
					unset( $children_elements[ $id ] );
			}

			if ( isset($newlevel) && $newlevel ){
				//end the child delimiter
				$cb_args = array_merge( array(&$output, $depth), $args);
				call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
			}

			//end this element
			$cb_args = array_merge( array(&$output, $element, $depth), $args);
			call_user_func_array(array(&$this, 'end_el'), $cb_args);

		}

	}

}
add_action( 'after_setup_theme', 'hippocket_bootstrap_setup' );


/*
 * Creating a function to create our CPT
*/

function custom_hippocket_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Features', 'Post Type General Name', 'hippocket' ),
		'singular_name'       => _x( 'Features', 'Post Type Singular Name', 'hippocket' ),
		'menu_name'           => __( 'Features', 'hippocket' ),
		'parent_item_colon'   => __( 'Parent Features', 'hippocket' ),
		'all_items'           => __( 'All Features', 'hippocket' ),
		'view_item'           => __( 'View Features', 'hippocket' ),
		'add_new_item'        => __( 'Add New Features', 'hippocket' ),
		'add_new'             => __( 'Add New', 'hippocket' ),
		'edit_item'           => __( 'Edit Features', 'hippocket' ),
		'update_item'         => __( 'Update Features', 'hippocket' ),
		'search_items'        => __( 'Search Features', 'hippocket' ),
		'not_found'           => __( 'Not Found', 'hippocket' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'hippocket' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'Features', 'hippocket' ),
		'description'         => __( 'Features about hippocket', 'hippocket' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail',),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		//'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
// Set UI labels for Custom Post Type
	$labelstwo = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'hippocket' ),
		'singular_name'       => _x( 'Testimonials', 'Post Type Singular Name', 'hippocket' ),
		'menu_name'           => __( 'Testimonials', 'hippocket' ),
		'parent_item_colon'   => __( 'Parent Testimonials', 'hippocket' ),
		'all_items'           => __( 'All Testimonials', 'hippocket' ),
		'view_item'           => __( 'View Testimonials', 'hippocket' ),
		'add_new_item'        => __( 'Add New Testimonials', 'hippocket' ),
		'add_new'             => __( 'Add New', 'hippocket' ),
		'edit_item'           => __( 'Edit Testimonials', 'hippocket' ),
		'update_item'         => __( 'Update Testimonials', 'hippocket' ),
		'search_items'        => __( 'Search Testimonials', 'hippocket' ),
		'not_found'           => __( 'Not Found', 'hippocket' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'hippocket' ),
	);
	
// Set other options for Custom Post Type
	
	$argstwo = array(
		'label'               => __( 'Testimonials', 'hippocket' ),
		'description'         => __( 'Testimonials user comments', 'hippocket' ),
		'labels'              => $labelstwo,
		// Testimonials this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		//'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
// Set UI labels for Custom Post Type
	$labelsthree = array(
		'name'                => _x( 'FAQ', 'Post Type General Name', 'hippocket' ),
		'singular_name'       => _x( 'FAQ', 'Post Type Singular Name', 'hippocket' ),
		'menu_name'           => __( 'FAQ', 'hippocket' ),
		'parent_item_colon'   => __( 'Parent FAQ', 'hippocket' ),
		'all_items'           => __( 'All FAQ', 'hippocket' ),
		'view_item'           => __( 'View FAQ', 'hippocket' ),
		'add_new_item'        => __( 'Add New FAQ', 'hippocket' ),
		'add_new'             => __( 'Add New', 'hippocket' ),
		'edit_item'           => __( 'Edit FAQ', 'hippocket' ),
		'update_item'         => __( 'Update FAQ', 'hippocket' ),
		'search_items'        => __( 'Search FAQ', 'hippocket' ),
		'not_found'           => __( 'Not Found', 'hippocket' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'hippocket' ),
	);
	
// Set other options for Custom Post Type
	
	$argsthree = array(
		'label'               => __( 'FAQ', 'hippocket' ),
		'description'         => __( 'FAQ user comments', 'hippocket' ),
		'labels'              => $labelsthree,
		// FAQ this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		//'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
// Set UI labels for Custom Post Type
	$labelsfour = array(
		'name'                => _x( 'Nations', 'Post Type General Name', 'hippocket' ),
		'singular_name'       => _x( 'Nations', 'Post Type Singular Name', 'hippocket' ),
		'menu_name'           => __( 'Nations', 'hippocket' ),
		'parent_item_colon'   => __( 'Parent Nations', 'hippocket' ),
		'all_items'           => __( 'All Nations', 'hippocket' ),
		'view_item'           => __( 'View Nations', 'hippocket' ),
		'add_new_item'        => __( 'Add New Nations', 'hippocket' ),
		'add_new'             => __( 'Add New', 'hippocket' ),
		'edit_item'           => __( 'Edit Nations', 'hippocket' ),
		'update_item'         => __( 'Update Nations', 'hippocket' ),
		'search_items'        => __( 'Search Nations', 'hippocket' ),
		'not_found'           => __( 'Not Found', 'hippocket' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'hippocket' ),
	);
	
// Set other options for Custom Post Type
	
	$argsfour = array(
		'label'               => __( 'Nations', 'hippocket' ),
		'description'         => __( 'Nations user comments', 'hippocket' ),
		'labels'              => $labelsfour,
		// Nations this CPT supports in Post Editor
		'supports'            => array( 'title',),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		//'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
// Set UI labels for Custom Post Type
	$labelsfive = array(
		'name'                => _x( 'Team', 'Post Type General Name', 'hippocket' ),
		'singular_name'       => _x( 'Team', 'Post Type Singular Name', 'hippocket' ),
		'menu_name'           => __( 'Team', 'hippocket' ),
		'parent_item_colon'   => __( 'Parent Team', 'hippocket' ),
		'all_items'           => __( 'All Team', 'hippocket' ),
		'view_item'           => __( 'View Team', 'hippocket' ),
		'add_new_item'        => __( 'Add New Team', 'hippocket' ),
		'add_new'             => __( 'Add New', 'hippocket' ),
		'edit_item'           => __( 'Edit Team', 'hippocket' ),
		'update_item'         => __( 'Update Team', 'hippocket' ),
		'search_items'        => __( 'Search Team', 'hippocket' ),
		'not_found'           => __( 'Not Found', 'hippocket' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'hippocket' ),
	);
	
// Set other options for Custom Post Type
	
	$argsfive = array(
		'label'               => __( 'Team', 'hippocket' ),
		'description'         => __( 'Team user comments', 'hippocket' ),
		'labels'              => $labelsfive,
		// Team this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		//'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
// Set UI labels for Custom Post Type
	/*$labelssix = array(
		'name'                => _x( 'Pricing', 'Post Type General Name', 'hippocket' ),
		'singular_name'       => _x( 'Pricing', 'Post Type Singular Name', 'hippocket' ),
		'menu_name'           => __( 'Pricing', 'hippocket' ),
		'parent_item_colon'   => __( 'Parent Pricing', 'hippocket' ),
		'all_items'           => __( 'All Pricing', 'hippocket' ),
		'view_item'           => __( 'View Pricing', 'hippocket' ),
		'add_new_item'        => __( 'Add New Pricing', 'hippocket' ),
		'add_new'             => __( 'Add New', 'hippocket' ),
		'edit_item'           => __( 'Edit Pricing', 'hippocket' ),
		'update_item'         => __( 'Update Pricing', 'hippocket' ),
		'search_items'        => __( 'Search Pricing', 'hippocket' ),
		'not_found'           => __( 'Not Found', 'hippocket' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'hippocket' ),
	);*/
	
// Set other options for Custom Post Type
	
	/*$argssix = array(
		'label'               => __( 'Pricing', 'hippocket' ),
		'description'         => __( 'Pricing user comments', 'hippocket' ),
		'labels'              => $labelssix,
		// Pricing this CPT supports in Post Editor
		'supports'            => array( ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		//'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);*/
	
	// Registering your Custom Post Type
	register_post_type( 'Features', $args );
	register_post_type( 'Testimonials', $argstwo );
	register_post_type( 'faq', $argsthree );
	register_post_type( 'nations', $argsfour );
	register_post_type( 'Team', $argsfive );
	//register_post_type( 'Pricing', $argssix );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'custom_hippocket_post_type', 0 );

// GET FEATURED IMAGE
function hip_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
        return $post_thumbnail_img[0];
    }
}
// ADD NEW COLUMN
function hip_columns_head($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function hip_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = hip_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" />';
        }
    }
}
add_filter('manage_features_posts_columns', 'hip_columns_head', 10);
add_action('manage_features_posts_custom_column', 'hip_columns_content', 10, 2);
add_filter('manage_team_posts_columns', 'hip_columns_head', 10);
add_action('manage_team_posts_custom_column', 'hip_columns_content', 10, 2);

function remove_allowed_html_tags_note( $defaults ) {
 //returns the modified array
 return array_replace( $defaults, array('comment_notes_after' => '' ) );
}
add_filter('comment_form_defaults' , 'remove_allowed_html_tags_note', 30);

 function my_display_gravatar() { 
    global $author;
    get_the_author_meta();
    // Get User Email Address
    $getuseremail = $author->user_email;
} 

function hippocket_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	 <div id="comment-<?php comment_ID(); ?>" class="media">
		<a class="pull-left" href="#"><?php echo get_avatar($comment,$size='48',$default=my_display_gravatar() ); ?></a>
		 <div class="media-body">
			 <?php printf(__('<h6 class="media-heading">%s</h6>'), get_comment_author()) ?>

			<?php if ($comment->comment_approved == '0') : ?>
			 <em><?php _e('Your comment is awaiting moderation.') ?></em>
			 <br />
			<?php endif; ?>

			<p class="text-muted"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?><?php edit_comment_link(__('(Edit)'),'  ','') ?></p>

			<?php comment_text() ?>

			<div class="reply">
			 <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			</div>
		</div>
	</div>
<?php
}

/**
 * remove_empty_tags_recursive ()
 * Remove the nested HTML empty tags from the string.
 *
 * @author    Junaid Atari <mj.atari@gmail.com>
 * @version    1.0
 * @param    string    $str    String to remove tags.
 * @param    string    $repto    Replace empty string with.
 * @return    string    Cleaned string.
 */
function remove_empty_tags_recursive ($str, $repto = NULL)
{
    //** Return if string not given or empty.
    if (!is_string ($str)
        || trim ($str) == '')
            return $str;

    //** Recursive empty HTML tags.
    return preg_replace (

        //** Pattern written by Junaid Atari.
        '/<([^<\/>]*)>([\s]*?|(?R))<\/\1>/imsU',

        //** Replace with nothing if string empty.
        !is_string ($repto) ? '' : $repto,

        //** Source string
        $str
    );
}
