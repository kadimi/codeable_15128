<!DOCTYPE html>
<html
lang="en-US">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<title><?php if ( is_category() ) {
		echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_tag() ) {
		echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_archive() ) {
		wp_title(''); echo ' Archive | '; bloginfo( 'name' );
	} elseif ( is_search() ) {
		echo 'Search for &quot;'.wp_specialchars($s).'&quot; | '; bloginfo( 'name' );
	} elseif ( is_home() || is_front_page() ) {
		bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
	}  elseif ( is_404() ) {
		echo 'Error 404 Not Found | '; bloginfo( 'name' );
	} elseif ( is_single() ) {
		wp_title('');
	} else {
		echo wp_title('');
	} ?>
</title>

<!-- Favicons -->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<?php wp_head(); global $hip_options; ?>
</head>
<body <?php if ( is_home() || is_front_page() ) { body_class('mob-app'); } else { body_class(); } ?>>
<div class="preloader"></div>
<?php if(is_front_page()){ ?>
 
<?php } ?>
<?php if(!is_front_page()){ ?>
 <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $hip_options['opt-logo-page']['url']; ?>" alt="logo"></a>
        </div>
        <div class="navbar-collapse collapse">
           <?php
			$args = array(
				'theme_location' => 'primary',
				'depth'      => 2,
				'container'  => false,
				'walker'     => new Bootstrap_Walker_Nav_Menu()
			);

			wp_nav_menu($args);
		  ?>
        </div><!--/.nav-collapse -->
      </div>
    </div>
<!--// Navbar Ends--> 
<?php } ?>