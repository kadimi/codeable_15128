 $(function () {
  $('[data-typer-targets]').typer();
  
  $('.scrollto, .gototop').bind('click',function(event){
		 var $anchor = $(this);
		 $('html, body').stop().animate({
         scrollTop: $($anchor.attr('href')).offset().top
          }, 1500,'easeInOutExpo');
     event.preventDefault();
  });
  
  $('.menu').addClass('nav');
  $('.menu').addClass('navbar-nav');
  $('.menu').addClass('navbar-right');
  
  $('.fn').addClass('media-heading');
  $('.comment-metadata').addClass('text-muted');
  
  $('.comment-reply-title').addClass('comments-head');
  
  $('.submit').addClass('btn');
  $('.submit').addClass('btn-primary');
  $('.submit').addClass('btn-lg');
  
  //sidebar
  $('.widget_archive').addClass('vertical-links');
  $('.widget_archive').addClass('has-margin-bottom');
  $('.widget_archive ul').addClass('list-unstyled');
  
  $('#text-2').addClass('well');
  $('#search-2').addClass('blog-search');
  $('#search-2').addClass('has-margin-bottom');
  
  $('.tg tr:nth-child(4)').prepend(' <td class="tg-031e td-hip" rowspan="9">Target Markets 2015</td>');
  //$('.tg tr:nth-child(4) .td-7').attr('rowspan', '9');
  
  $('.responsive').not('table').responsiveText();
  $('table.responsive').responsiveTables();
  
  $('.remove-p').responsiveEqualHeightGrid();
  $('body > .container').css('height', 'auto');
});