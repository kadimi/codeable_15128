<?php get_header(); ?>
<!--SUBPAGE HEAD-->

<div class="subpage-head">
  <div class="container">
    <div class="section-title">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
	<?php endwhile; ?>
    </div>
  </div>
</div>

<!-- // END SUBPAGE HEAD -->

<div class="container">
  <div class="row">
    <div class="col-md-9 has-margin-bottom charcoal"> 
   <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <!--Page-->
      
         <?php the_content(); ?>
    
	<?php endwhile; ?>
    </div>
    <!--// col md 9--> 
    
   <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>