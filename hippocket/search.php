<?php get_header(); ?>
<!--SUBPAGE HEAD-->

<div class="subpage-head">
  <div class="container">
    <div class="section-title">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <h2><?php printf( __( 'Search Results for: %s', 'vantage' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
	<?php endwhile; ?>
    </div>
  </div>
</div>

<!-- // END SUBPAGE HEAD -->

<div class="container">
  <div class="row">
    <div class="col-md-9 has-margin-bottom charcoal"> 
   <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <!--Page-->
      
        <div class="row has-margin-bottom">
			<div class="col-md-4 col-sm-4"> <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. 
				the_post_thumbnail();
			}else{  ?> <img class="img-responsive center-block" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog-thumb-1.jpg" alt="bulletin blog"> <?php } ?></div>
			<div class="col-md-8 col-sm-8 bulletin">
			  <h5 class="media-heading"><?php the_title(); ?></h5>
			  <p><?php _e('on '); the_time('F j, Y'); _e(', by '); the_author_posts_link(); ?></p>
			  <p><?php the_excerpt(); ?></p>
			  <a class="btn btn-primary" href="<?php the_permalink(); ?>" role="button">Read Article ?</a> </div>
		</div>
    
	<?php endwhile; ?>
    </div>
    <!--// col md 9--> 
    
   <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>