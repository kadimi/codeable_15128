<?php
/**
 * The template for displaying search forms in hippocket
 *
 * @package hippocket
 * @since hippocket 1.0
 * @license GPL 2.0
 */
?>

<form method="get" id="searchform" class="input-group input-group-lg" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<input type="text" class="form-control" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php esc_attr_e( 'Search', 'hippocket' ); ?>" />
	<span class="input-group-btn">
		<input type="submit" class="btn btn-default" name="submit" id="searchsubmit" value="<?php esc_attr_e( '', 'hippocket' ); ?>" />
		<i class="glyphicon glyphicon-search glyphicon-lg"></i>
	</span>
</form>

