 <!--Blog Sidebar-->
<div class="col-md-3">
<?php do_action( 'before_sidebar' ); ?>
<?php dynamic_sidebar( 'sidebar-1' ); ?>
<?php do_action( 'after_sidebar' ); ?>

<?php if(is_page('blog') || is_single() || is_page('hippocket-ambassador')){ ?>
<!-- Link Through Buttons -->
<div class="col-md-12" style="margin-top:0; margin-bottom:0; padding:0;">
  <div class="multiple-links text-center has-margin-vertical">  
	<a href="<?php global $hip_options; echo $hip_options['opt-early-access-url']; ?>"class="btn btn-success btn-lg gototop"><?php echo $hip_options['opt-early-access']; ?></a>       
 </div>
</div>
<!-- // Link Through Buttons Ends -->  
<?php } ?>
</div>