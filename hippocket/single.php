<?php
/*
Template Name: Blog
*/
get_header(); ?>
<!--SUBPAGE HEAD-->

<div class="subpage-head">
  <div class="container">
    <div class="section-title">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
      <h4><?php _e('on '); the_time('F j, Y'); _e(', by '); the_author(); ?></h4>
	<?php endwhile; ?>
    </div>
  </div>
</div>

<!-- // END SUBPAGE HEAD -->

<div class="container">
  <div class="row">
    <div class="col-md-9 has-margin-bottom charcoal"> 
	<article class="blog-content">
   <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <!--Blog list-->
        <?php the_content(); ?>
		
	  <!--Blog List End -->
	<?php endwhile; ?>
	</article>
	
	<section class="post-comment-form">
        <?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		?>
    </section>
    </div>
    <!--// col md 9--> 
    
   <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>