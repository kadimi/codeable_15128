<?php 
/*
Template Name: About
*/
get_header(); ?>
<!--SUBPAGE HEAD-->

<div class="subpage-head">
  <div class="container">
    <div class="section-title">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
	<?php endwhile; ?>
    </div>
  </div>
</div>

<!-- // END SUBPAGE HEAD -->

<div class="container">
  <div class="row">
    <div class="col-md-9 has-margin-bottom charcoal"> 
   <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <!--Page-->
      
         <?php the_content(); ?>
    
	<?php endwhile; ?>
	<!-- Link Through Buttons -->
	  <div class="col-md-12" style="margin-left:0; padding-left:0;">
		  <div class="multiple-links has-margin-vertical" style="margin-left:0; padding-left:0;">  
			<a href="<?php echo $hip_options['opt-early-access-url']; ?>"class="btn btn-success btn-lg gototop"><?php echo $hip_options['opt-early-access']; ?></a>       
		 </div>
	</div>
    <!-- // Link Through Buttons Ends --> 
	<?php
	$temp = $wp_query; 
	$wp_query = null; 
	$wp_query = new WP_Query(); 
	$wp_query->query('showposts=-1&post_type=team&order=asc'); 
	while ($wp_query->have_posts()) : $wp_query->the_post(); 
	?>
	<div class="row has-margin-bottom" style="margin-top:40px;">
		<div class="col-md-4 col-sm-4 about"> <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. 
					the_post_thumbnail('thumb-about');
				}else{  ?> <!--<img class="img-responsive center-block" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog-thumb-1.jpg" alt="bulletin blog">--> <?php } ?>
		</div>
		<div class="col-md-8 col-sm-8 bulletin">
		  <h6 class="media-heading"><?php the_title(); ?></h6>
		  <p><?php the_content(); ?></p>
		</div>
	</div>	
	<?php endwhile; $wp_query = null; $wp_query = $temp;  // Reset ?>
    </div>
    <!--// col md 9--> 
    
   <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>