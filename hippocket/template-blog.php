<?php
/*
Template Name: Blog
*/
get_header(); ?>
<!--SUBPAGE HEAD-->

<div class="subpage-head">
  <div class="container">
    <div class="section-title">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
	<?php endwhile; ?>
    </div>
  </div>
</div>

<!-- // END SUBPAGE HEAD -->

<div class="container">
  <div class="row">
    <div class="col-md-9 has-margin-bottom charcoal"> 
    <?php
		$temp = $wp_query; 
		$wp_query = null; 
		$wp_query = new WP_Query(); 
		$wp_query->query('showposts=4&post_type=post'); 
		while ($wp_query->have_posts()) : $wp_query->the_post(); 
	?>
      <!--Blog list-->
      
      <div class="row has-margin-bottom">
        <div class="col-md-12 col-sm-12 bulletin">
          <h5 class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
          <p><?php _e('on '); the_time('F j, Y'); _e(', by '); the_author(); ?></p>
		  <?php echo do_shortcode('[shareaholic app="share_buttons" id="15338823"]') ?>
          <p><?php the_excerpt(); ?></p>
          <a class="btn btn-primary" href="<?php the_permalink(); ?>" role="button">Read More</a> 
		</div>
      </div>
      
      <!-- PAGINATION --
      
      <div class="text-center center-block">
        <ul class="pagination">
          <li class="disabled"><a href="#">�</a></li>
          <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">�</a></li>
        </ul>
      </div>-->
	<?php endwhile; ?>
	<div class="paginate">
		<?php //kriesi_pagination(); ?>
	</div>
	<?php
	$wp_query = null; 
	$wp_query = $temp;  // Reset 
	?>
    </div>
    <!--// col md 9--> 
    
   <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>