<?php 
/*
Template Name: Homepage
*/
get_header(); ?>
<!-- Parallax Header Starts -->
<main id="top" class="masthead" role="main">
  <div class="container"> 
	<!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-static-top navbar-home" role="navigation" style="background:none; border:0;">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
           <?php
			$args = array(
				'theme_location' => 'primary',
				'depth'      => 2,
				'container'  => false,
				'walker'     => new Bootstrap_Walker_Nav_Menu()
			);

			wp_nav_menu($args);
		  ?>
        </div><!--/.nav-collapse -->
      </div>
    </div>
<!--// Navbar Ends--> 
    <!-- Startup Logo -->
    <div class="logo"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $hip_options['opt-logo-home']['url']; ?>" alt="hippocket-logo" class="img-responsive"></a> </div>
    <!-- Hero Title -->
    <h1 class="movingtextt"><?php echo $hip_options['opt-big-editor']; ?></h1>
    <!-- Link Through Buttons -->
    <div class="row">
	  <div class="col-md-12">
		  <div class="multiple-links text-center has-margin-vertical">  
			<a href="<?php echo $hip_options['opt-early-access-url']; ?>"class="btn btn-success btn-lg gototop"><?php echo $hip_options['opt-early-access']; ?></a>       
		 </div>
    </div>
	  </div>
    <!-- // Link Through Buttons Ends --> 
    
    <!-- Banner Image -->
    <div class="row">
    <div class="col-md-12">
    
   <img src="<?php echo $hip_options['opt-iphone-one']['url']; ?>" alt="Hippocket Banner" class="img-responsive center-block">
    
    </div>
    
      
    </div>
    <!-- // Banner Image Ends --> 

  </div>
</main>
<!-- // Parallax Header Ends --> 
<!-- Container -->
<div class="container" id="explore"> 
  <!-- Hero Image -->
  <div class="section-title home-title">
    <h2><?php echo $hip_options['opt-after-white-iphone']; ?></h2>
    <h4><?php echo $hip_options['opt-subtitle-white-iphone-text']; ?></h4>
  </div>
  <section class="row heroimg breath">
    <div class="col-md-6 text-center"> <img src="<?php echo $hip_options['opt-iphone-two']['url']; ?>" alt="Mobile Mockup"></div>
  
	<div class="col-md-6">
		<div class="contents" id="div_33b3_0">
			<p id="p_33b3_0"><b>Work Smarter</b></p>
			<p id="p_33b3_1"><font color="#808080">Spend more time prospecting, less on promotion and admin</font></p>

			<p id="p_33b3_3"><b>Instant Marketing</b></p>
			<p id="p_33b3_4"><font color="#808080">Market hip pockets and pre-MLS properties instantly</font></p>

			<p id="p_33b3_6"><b>Not an MLS</b></p>
			<p id="p_33b3_7"><font color="#808080">Communicate with the Community not just a database</font></p>

			<p id="p_33b3_9"><b>Fast &amp; Social</b></p>
			<p id="p_33b3_10"><font color="#808080">Engage with thousands of Realtors in your Community</font></p>

			<p id="p_33b3_12"><b>Focused</b></p>
			<p id="p_33b3_13"><font color="#808080">Targeted, relevant marketing</font></p>

			<p id="p_33b3_15"><b>Efficient</b></p>
			<p id="p_33b3_16"><font color="#808080">One listing. One conversation</font></p>

			<p id="p_33b3_18"><b>Intelligent</b></p>
			<p id="p_33b3_19"><font color="#808080">Instantly see the closest match to your listing</font></p>
		</div>
	</div>
  
  </section>
  <!-- //Hero Image --> 
  
<!-- Link Through Buttons -->
<div class="col-md-12" style="margin-top:0; margin-bottom:0;">
  <div class="multiple-links text-center has-margin-vertical">  
	<a href="<?php echo $hip_options['opt-early-access-url']; ?>"class="btn btn-success btn-lg gototop"><?php echo $hip_options['opt-early-access']; ?></a>       
 </div>
</div>
<!-- // Link Through Buttons Ends --> 

  <!-- Features Section -->
  <div class="section-title home-title">
    <h2><?php echo $hip_options['opt-features']; ?></h2>
	<h4>Launching with the basics and growing from there</h4>
  </div>
  <section class="row features"> 

	<?php 
	$counter = 1;
	$args = array( 'post_type' => 'features', 'posts_per_page' => 8, 'order'   => 'ASC', );
	$the_query = new WP_Query( $args ); 
	?>
	<?php if ( $the_query->have_posts() ) : ?>
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<!-- Features <?php echo $counter; ?>-->
		<div class="col-sm-6 col-md-3" id="post-<?php the_ID(); ?>">
		  <div class="thumbnail"> 
			<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. 
				the_post_thumbnail();
			} ?>
			<div class="caption">
			  <h3><?php the_title(); ?></h3>
			  <?php the_content(); ?> 
			</div>
		  </div>
		</div>
	<?php 
	  if ($counter % 4 == 0) {
	 $str = '</section><div class="section-title home-title more-to-come-'.$counter.'"><h4>More to come in 2015</h4></div><section class="row features counter-'.$counter.'">';
	 echo remove_empty_tags_recursive ($str);
	}
	$counter++; endwhile; wp_reset_postdata(); endif; ?>
  </section>
  <!-- // Features Section Ends --> 
  
<!-- Link Through Buttons -->
<div class="col-md-12" style="margin-top:0; margin-bottom:0;">
  <div class="multiple-links text-center has-margin-vertical">  
	<a href="<?php echo $hip_options['opt-early-access-url']; ?>"class="btn btn-success btn-lg gototop"><?php echo $hip_options['opt-early-access']; ?></a>       
 </div>
</div>
<!-- // Link Through Buttons Ends -->   

</div>
<!-- // Container Ends --> 
<?php $show = $hip_options['opt-testimonial-show']; if($show == 1){ ?>
<!-- Grey Highlight Section -->
<div class="highlight testimonials">
  <div class="container"> 
    
    <!-- Testimonials -->
    <div class="section-title home-title">
      <h2><?php echo $hip_options['opt-testimonial-text']; ?></h2>
      <h4><?php echo $hip_options['opt-testimonial-subtitle']; ?> </h4>
    </div>
    <section class="row breath">
	<?php 
	$args = array( 'post_type' => 'testimonials', 'posts_per_page' => 2, 'order'   => 'ASC', );
	$the_query = new WP_Query( $args ); 
	?>
	<?php if ( $the_query->have_posts() ) : ?>
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <div class="col-md-6">
        <div class="testblock"><?php the_content(); ?></div>
        <div class="clientblock"> 
			<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. 
				the_post_thumbnail();
			} ?>
          <p><strong><?php the_title(); ?></strong> <br> <?php $data = rwmb_meta( 'hippocket_wysiwyg' ); echo strip_tags($data); ?></p>
        </div>
      </div>
	<?php endwhile; wp_reset_postdata(); endif; ?>
    </section>
    <!-- // End Testimonials --> 
    
  </div>
</div>
<!-- // End Grey Highlight Section --> 
<?php } ?>
<!-- Container -->
<div class="container"> 
  
  <!-- Pricing -->
  <div class="section-title home-title">
    <h2><?php echo $hip_options['opt-pricing-title']; ?></h2>
    <h4><?php echo $hip_options['opt-pricing-subtitle']; ?> </h4>
  </div>
  <section class="row breath planpricing">
    <div class="col-md-4" style="cursor:pointer" onclick='location.href="<?php echo $hip_options['opt-agent-url']; ?>"'>
      <div class="pricing color1">
        <div class="planname"><?php echo $hip_options['opt-agent']; ?></div>
        <div class="price"><?php echo $hip_options['opt-agent-price']; ?></div>
		<div class="seasons text-center">(Sign up for Early Access to our beta)</div>
      </div>
	  <div class="billing"><?php echo $hip_options['opt-agent-about']; ?></div>
    </div>
    <div class="col-md-4" style="cursor:pointer" onclick='location.href="<?php echo $hip_options['opt-agent-pro-url']; ?>"'>
      <div class="pricing color2">
        <div class="planname"><?php echo $hip_options['opt-agent-pro']; ?></div>
        <div class="price"><span class="curr">$</span><?php echo $hip_options['opt-agent-pro-price']; ?><span class="per">/MO</span></div>
		<div class="seasons text-center">(Summer 2015)</div>
	 </div>
	  <div class="billing"><?php echo $hip_options['opt-agent-pro-about']; ?></div>
    </div>
    <div class="col-md-4" style="cursor:pointer" onclick='location.href="<?php echo $hip_options['opt-broker-url']; ?>"'>
      <div class="pricing color3">
        <div class="planname"><?php echo $hip_options['opt-broker']; ?></div>
        <div class="price"> <span class="curr">$</span><?php echo $hip_options['opt-brober-price']; ?><span class="per">/MO</span></div>
        <div class="seasons text-center">(Late 2015)</div>
      </div>
	  <div class="billing"><?php echo $hip_options['opt-broker-about']; ?></div>
    </div>
  </section>
  <!-- // End Pricing --> 
	<section class="row breath">
		<?php ob_start(); ?>
		<table class="table tgg responsive" data-height="100%" data-min="10" data-max="25" cellpadding="0" cellspacing="0">
			<thead>
			  <tr style="text-align:center;">
				<th class="price-head">Features</th>
				<th class="price-head">Agent<br><span>Free</span><br/><small>Sign up for our Early Access beta</small></th>
				<th class="price-head" style="background-color: #56bed4;">Agent Pro<br><span>$9/mo</span><br/><small>Summer 2015</small></th>
				<th class="price-head" style="background-color: #56bed4;">Broker<br><span>$45-99/mo</span><br/><small>Late 2015</small></th>
			  </tr>
			</thead>
			  <tr>
				<td class="price-name">User Profile</td>
				<td class="tickteal"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Post & Edit a HipPocket<br/>
			<small>(For Sale, Buyer Need, For Rent, Renter Need)</small></td>
				<td class="tickteal"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Like, Comment, Share a HipPocket</td>
				<td class="tickteal"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Message HipPocket User</td>
				<td class="tickteal"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Notifications</td>
				<td class="tickteal"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Search</td>
				<td class="tickteal"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Follow a HipPocket Area</td>
				<td class="tickteal"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Follow a HipPocket User</td>
				<td class="tickteal"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Number of Photos per listing</td>
				<td class="tickteal">1</td>
				<td class="tick">25</td>
				<td class="tick">25</td>
			  </tr>
			   <tr>
				<td class="price-name">Reposting of HipPockets</td>
				<td class="tickteal">-</td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Create Private Groups</td>
				<td class="tickteal">-</td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">HipPocket Matchmaking<br/>
			<small>(For Sale to Buyer Need; For Rent to Renter Need)</small></td>
				<td class="tickteal">-</td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Build & Manage a Broker Page</td>
				<td class="tickteal">-</td>
				<td class="tick">-</td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
			   <tr>
				<td class="price-name">Create Private Broker/Agents Group</td>
				<td class="tickteal">-</td>
				<td class="tick">-</td>
				<td class="tick"><font color="#ffffff">✓</font><br></td>
			  </tr>
		</table>
		<?php ob_end_clean(); ?>

		<!-- Link Through Buttons -->
		<div class="col-md-12" style="margin-top:0; margin-bottom:0;">
		  <div class="multiple-links text-center has-margin-vertical">  
			<a href="<?php echo $hip_options['opt-early-access-url']; ?>"class="btn btn-success btn-lg gototop"><?php echo $hip_options['opt-early-access']; ?></a>       
		 </div>
		</div>
		<!-- // Link Through Buttons Ends -->  
	</section>
  
	<!-- Nationwide -->
	<div class="section-title home-title">
		<h2><?php echo $hip_options['opt-nationwide-title']; ?></h2>
		<h4><?php echo $hip_options['opt-nationawide-subtext']; ?></h4>
	</div>
	<!--  
	<section class="row faq breath" style="text-align:center;">
		<table class="tg">
		 
			 <th class="tg-031e" rowspan="3">Launch Markets Spring 2015</th>  
			<!--<div class="col-md-6">--
			<?php 
			/*$counter = 1;
			$args = array( 'post_type' => 'nations', 'posts_per_page' => 16, 'order'   => 'ASC', );
			$the_query = new WP_Query( $args ); 
			?>
			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			 <td class="tg-031e td-<?php echo $counter; ?>"><?php the_title(); ?></td>

			 <?php 
			  if ($counter % 2 == 0) {
			  echo '<!--</div><div class="col-md-6">--><tr>';
			}
			$counter++; endwhile; wp_reset_postdata(); endif;*/ ?>
			</tr>
			<!--</div>--
		</table>
	</section>
	<!-- Nationwide End-->
 
	<?php ob_start(); ?>
  <!-- FAQ -->
  <div class="section-title home-title">
    <h5>Frequently Asked Questions</h5>
  </div>
  <section class="row faq breath">
   
	<?php 
	$counter = 1;
	$args = array( 'post_type' => 'faq', 'posts_per_page' => -1, 'order'   => 'ASC', );
	$the_query = new WP_Query( $args ); 
	?>
	<?php if ( $the_query->have_posts() ) : ?>
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<div class="col-sm-6 col-md-6 remove-p faq-<?php echo $counter++; ?>">
	
      <h6><?php the_title(); ?></h6>
	  <?php the_content(); ?>    
	</div>
	<?php $counter++; endwhile; wp_reset_postdata(); endif; ?>
  <!-- // End FAQ --> 
	<?php ob_end_clean(); ?>

	  <!-- Link Through Buttons -->
	<div class="col-md-12" style="margin-top:0; margin-bottom:0;">
	  <div class="multiple-links text-center has-margin-vertical">  
		<a href="<?php echo $hip_options['opt-early-access-url']; ?>"class="btn btn-success btn-lg gototop"><?php echo $hip_options['opt-early-access']; ?></a>       
	 </div>
	</div>
	<!-- // Link Through Buttons Ends --> 
	</section>
</div>
<!-- // Container Ends --> 

<!-- Parallax Footer Starts -->
<main class="footercta" role="main">
  <div class="container"> 
     <!-- Startup Logo -->
    <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $hip_options['opt-logo-home']['url']; ?>" alt="hippocket-logo" class="img-responsive"></a></div>
    <!-- Hero Title -->
    <h1><?php echo $hip_options['opt-big-editor']; ?></h1>
          <!-- Link Through Buttons -->
    <div class="row">
		<div class="col-md-12">
		  <div class="multiple-links text-center has-margin-vertical">  
			<a href="<?php echo $hip_options['opt-early-access-url']; ?>"class="btn btn-success btn-lg gototop"><?php echo $hip_options['opt-early-access']; ?></a>       
		  </div>
		</div>
    </div>
    <!-- // Link Through Buttons Ends --> 
  </div>
</main>
<!-- // Parallax Footer Ends --> 
<?php get_footer(); ?>